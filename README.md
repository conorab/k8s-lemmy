# k8s-lemmy

See LICENSE file. This is licensed as such due to the two files from the official Lemmy project mentioned below, which are provided under this licence.

My Kubernetes configs for running Lemmy. All sensitive information has been removed from the configs (including the base64-encoded config in lemmy-confighjson.yml) and replaced with generic data: e.g. "<lemmy db password>". Some configs specific to my site such as the SMTP server address, SMTP server address cluster IP mapping (needed if your mail server runs in the same cluster so DNS maps properly), the Lemmy instance name and URL have been left in to make it easier to know which values need to be changed in the lemmy.yml and lemmy-confighjson.yml files.

The base64-encoded file in 'lemmy-confighjson.yml' is based off: https://github.com/LemmyNet/lemmy/blob/1e99e8b9dc48b23fca3b1801c66f30cf17766825/config/defaults.hjson
The main deployment was adapted based on: https://github.com/LemmyNet/lemmy/blob/40ff77eee2836b222ebd488b30b1d06b685940ee/docker/docker-compose.yml and some troubleshooting (I believe the command for Pictrs needed to be altered).

This service has been configured to run in the default namespace. Your will want to change all values in the configs which look like: <name of value to be changed> (e.g. <lemmy db password> becomes password123). You will also want to change anything which has these as a value:
* lemmy.conorab.com
* mail.conorab.com:25
* 10.109.118.8 (the cluster IP of mail.conorab.com)
* /dev/windtunnel1/lemmy-pictrs-mnt (the block device formatted EXT4 holding the Pictrs data)
* /dev/windtunnel1/lemmy-postgres (the block device formatted EXT4 holding the Postgres DB data)
